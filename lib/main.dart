import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'layout',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'First layout'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final myController_username = TextEditingController();//Create a TextEditingController
  final myController_password = TextEditingController();
  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController_username.dispose();
    myController_password.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        color: Colors.lightBlueAccent[100],
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Icon(
                Icons.insert_photo,
                size: 200,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    'Row child 1',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    'Row child 1',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    'Row child 1',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(30),
                child: Text(
                  'Tis is column',
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                 controller: myController_username,
                  decoration: InputDecoration(
                    hintText: 'Userman',
                    fillColor: Colors.white54,
                    filled: true,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, right:  15, bottom: 30),
                child: TextField(
                  controller: myController_password,
                  decoration: InputDecoration(
                    hintText: 'Password',
                    fillColor: Colors.white54,
                    filled: true,
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary:  Colors.blue,//สี foreground
                      onPrimary: Colors.white,//สี เมื่อคลิก
                      padding:
                      EdgeInsets.symmetric(horizontal: 30,vertical: 15),
                      textStyle: TextStyle(fontSize: 20),

                    ),
                      onPressed: (){
                      myController_username.clear();// ปุ่มเครียดค่า
                      myController_password.clear();// ปุ่มเครียดค่า
                      },
                      child: Text('Cacel'),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary:  Colors.blue,//สี foreground
                      onPrimary: Colors.white,//สี เมื่อคลิก
                      padding:
                      EdgeInsets.symmetric(horizontal: 30,vertical: 15),
                      textStyle: TextStyle(fontSize: 20),

                    ),
                    onPressed: () => displayToast(),//print('Hell Login!!!'),
                    child: Text('Login'),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }// end build

void  displayToast(){
    String username = myController_username.text;
    String password = myController_password.text;
 Fluttertoast.showToast(
     msg: 'Your username: $username  \n Your username: $password',
   toastLength: Toast.LENGTH_SHORT,

 );
}

}// end class _myHomepage
